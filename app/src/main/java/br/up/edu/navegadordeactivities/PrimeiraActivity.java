package br.up.edu.navegadordeactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class PrimeiraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primeira);

        Intent intent = getIntent();
        String origem = intent.getStringExtra("id");

        TextView caixaTxt = (TextView) findViewById(R.id.txtT1);
        caixaTxt.setText(origem);

    }


    public void avancar(View v){

        Random r = new Random();
        int numero = r.nextInt(4);

        Intent intent = null;
        switch (numero){
            case 0:
                intent = new Intent(this, SegundaActivity.class);
                break;
            case 1:
                intent = new Intent(this, TerceiraAcitivity.class);
                break;
            case 2:
                intent = new Intent(this, QuartaActivity.class);
                break;
            case 3:
                intent = new Intent(this, QuintaActivity.class);
                break;
        }
        intent.putExtra("id", "1");
        startActivity(intent);
    }

    public void voltar(View v){

        finish();

    }
}
